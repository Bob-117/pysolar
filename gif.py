from PIL import Image
import glob


#register frames
# frame_count = 0
# while pygame loop:
#     frame_count += 1
#     filename = "screen_%04d.png" % frame_count
#     print(filename)
#     pygame.image.save(self.screen, os.path.join(path, filename))


# Create the frames
frames = []
imgs = glob.glob("gif/*.png")
print(len(imgs))
for index, i in enumerate(imgs):
    if index % 2 == 0:
        new_frame = Image.open(i)
        frames.append(new_frame)

# Save into a GIF file that loops forever
frames[0].save('solar.gif', format='GIF',
               append_images=frames[1:],
               save_all=True,
               duration=40, loop=0)
