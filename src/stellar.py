import math


class Stellar:
    def __init__(self, x, y, vx, vy, w, name):
        self.name = name
        self.x = float(x)
        self.y = float(y)
        self.vx = float(vx)
        self.vy = float(vy)
        self.w = float(w)

    def __str__(self):
        return f'pos : {self.x:.2f}-{self.y:.2f} / v : {self.vx:.2f}-{self.vy:.2f}'

    def calculate(self, others):
        dt = 1
        friction = 1

        f = self.force(others)  # current F between self & other planets

        # Update acceleration
        # 2nd Newton law : F = m*a
        ax = f[0] / self.w
        ay = f[1] / self.w

        # update position
        # equation of motion : x = x + vx' + 1/2ax'' =  x + vx' + F/m * t^2
        self.x = self.x + self.vx * dt + ax * dt * dt * (1 / 2)
        self.y = self.y + self.vy * dt + ay * dt * dt * (1 / 2)

        # update velocity
        self.vx = friction * (self.vx + ax * dt)
        self.vy = friction * (self.vy + ay * dt)
        print(self)
        # return (self.x + 1) % 50, (self.y + 1) % 50

    def force(self, other):
        listPlanets = other or []
        f = [0.0, 0.0]  # fx, fy
        G = 20.0
        for planet in listPlanets:
            if self != planet:
                # d = sqrt((x2-x1)²+(y2-y1)²)
                # distance = math.sqrt((planet.x - self.x) ** 2 + (planet.y - self.y) ** 2)
                distance = self.distance(planet)
                # print('==========================               ', distance)

                #  F = G * (m1 * m2) / (r^2)
                gravitational_force = G * self.w * planet.w / (distance ** 3)

                f[0] += gravitational_force * (planet.x - self.x)
                f[1] += gravitational_force * (planet.y - self.y)
        return f


    def distance(self, other):
        return math.sqrt((other.x - self.x) ** 2 + (other.y - self.y) ** 2)
