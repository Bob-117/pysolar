import os
from functools import singledispatch
from typing import overload

import pygame

from threading import Lock, Thread

from pygame import KEYDOWN, K_ESCAPE, QUIT

from src.stellar import Stellar


class SingletonMeta(type):
    """
    This is a thread-safe implementation of Singleton.
    """

    _instances = {}

    _lock: Lock = Lock()
    """
    We now have a lock object that will be used to synchronize threads during
    first access to the Singleton.
    """

    def __call__(cls, *args, **kwargs):
        """
        Possible changes to the value of the `__init__` argument do not affect
        the returned instance.
        """
        with cls._lock:
            if cls not in cls._instances:
                instance = super().__call__(*args, **kwargs)
                cls._instances[cls] = instance
        return cls._instances[cls]


class Window(metaclass=SingletonMeta):
    title: str = 'Solar'
    # Colours
    BACKGROUND = (255, 255, 255)
    FULLRED = (255, 0, 0)
    FULLGREEN = (0, 255, 0)
    FULLBLUE = (0, 0, 255)
    YELLOW = (255, 255, 0)
    # Game Setup
    FPS = 60
    fpsClock = pygame.time.Clock()
    WINDOW_WIDTH = 500
    WINDOW_HEIGHT = 500
    SUN = [Stellar(x=250, y=250, vx=0, vy=0, w=10, name='SUN')]

    def __init__(self):
        pygame.display.set_caption(self.title)
        taille = [500, 500]
        self.screen = pygame.display.set_mode(taille)
        self.clock = pygame.time.Clock()

    def reset(self):
        self.screen.fill(self.BACKGROUND)
        pygame.draw.circle(self.screen, self.YELLOW, (250, 250), 10, 10)

    def update(self):
        # pygame.display.update()
        # self.fpsClock.tick(self.FPS)
        pygame.display.flip()
        self.clock.tick(30)

    @staticmethod
    def event_handler_stop():
        for event in pygame.event.get():
            return event.key == K_ESCAPE if event.type == KEYDOWN else (event.type == QUIT or event.type == 32787)

    def draw(self, obj) -> None:
        self.reset()
        obj.calculate(self.SUN)
        if isinstance(obj, Stellar):
            pygame.draw.circle(self.screen, self.FULLBLUE, (self.resize(obj.x), self.resize(obj.y)), 3, 3)
        else:
            pygame.draw.circle(self.screen, self.FULLBLUE, (250, 250), 3, 3)
        self.update()

    def loop(self, something):
        while not self.event_handler_stop():
            # do something
            ticks = pygame.time.get_ticks()
            print(f'{ticks * 1e-3:.2f}')
            # print(self.clock.get_time())

            self.draw(something)


    def resize(self, value):
        if value == self.WINDOW_WIDTH:
            return value
        # if value > 400:
        #     value = 0
        # if value < 0:
        #     value = 400
        return value
