from decimal import Decimal

import numpy as np

from src import Stellar


# strength
class Solar:

    def __init__(self, stellar_list):
        self.G = 6.6743015E-11
        self.stellar_list = stellar_list
        self.nb = len(self.stellar_list)
        self.main_mat = np.empty([self.nb + 1, self.nb + 1], dtype='<U15')
        self.dist_mat = np.zeros(shape=(self.nb, self.nb))
        self.strength_mat = np.zeros(shape=(self.nb, self.nb))

    def fill_planet_name(self):
        for i, planet in enumerate(self.stellar_list):
            self.main_mat[i + 1, 0] = planet.name
            for j, distant_planet in enumerate(self.stellar_list):
                self.main_mat[0, j + 1] = distant_planet.name

    def resize_for_print(self):
        max_len = max(len(name) for name in self.main_mat.flatten())
        for i in range(self.nb + 1):
            for j in range(self.nb + 1):
                self.main_mat[i, j] = self.main_mat[i, j].rjust(15)

    def fill_dist_mat(self):
        for i, planet in enumerate(self.stellar_list):
            for j, distant_planet in enumerate(self.stellar_list):
                if planet.name != distant_planet.name:
                    self.main_mat[i + 1, j + 1] = '%.2E' % Decimal(planet.distance(distant_planet))
                    self.dist_mat[i, j] = planet.distance(distant_planet)
        self.main_mat[0, 0] = 'Distance (m)'
        self.resize_for_print()

    def fill_strength_mat(self):
        """
        Newton's law of universal gravitation.
        F = G * (m1 * m2) / r ^ 2
        F (N): gravitational force
        G (m^3 kg^-1 s^-2): gravitational constant
        m1, m2: (kg) masses of the objects
        r (m): distance between the objects
        :return:
        """
        for i, planet in enumerate(self.stellar_list):
            for j, distant_planet in enumerate(self.stellar_list):
                if planet.name != distant_planet.name:
                    current_strength = self.G * (planet.w * distant_planet.w) / self.dist_mat[i][j] ** 2
                    # m3 kg-1 s-2 * kg * kg * m-2
                    # m1 kg1 s-2
                    self.main_mat[i + 1, j + 1] = '%.2E' % Decimal(current_strength)
                    self.strength_mat[i, j] = current_strength
        self.main_mat[0, 0] = 'Strength (N)'
        self.resize_for_print()

    def main(self):
        self.fill_planet_name()
        self.fill_dist_mat()
        print(self.main_mat)
        self.fill_strength_mat()
        print(self.main_mat)


if __name__ == '__main__':
    hehe = [
        Stellar(x=250, y=250, vx=0, vy=0, w=10, name='gibson'),
        Stellar(x=300, y=250, vx=0, vy=0, w=10, name='neelix'),
        Stellar(x=250, y=350, vx=0, vy=0, w=10, name='mordecai')
    ]
    s = Solar(hehe)
    # for each planet in hehe except SUN : calculate move plot
    s.main()
