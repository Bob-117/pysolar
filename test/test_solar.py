import numpy as np

from src import Stellar, Solar

# testing environment :
#
# dist = sqrt((x2-x1)²+(y2-y1)²) = sqrt(100²+0) = 100
#
# F = G * (m1 * m2) / r ^ 2 = G * 100 / 10_000 = Ge-2
#
#
HEHE = [
    Stellar(x=0, y=0, vx=0, vy=0, w=10, name='gibson'),
    Stellar(x=0, y=100, vx=0, vy=0, w=10, name='neelix'),
]


def test_solar_init():
    _list = []
    _s = Solar(_list)
    assert isinstance(_s, Solar)


def test_len_and_shape():
    _s = Solar(HEHE)
    assert _s.nb == 2
    assert _s.main_mat.shape == (3, 3)
    assert _s.dist_mat.shape == (2, 2)
    assert _s.strength_mat.shape == (2, 2)


def test_solar_dist():
    _s = Solar(HEHE)
    _s.fill_dist_mat()
    assert _s.dist_mat.shape == (2, 2)
    np.testing.assert_array_equal(_s.dist_mat,
                                  [[0., 100.],
                                   [100., 0.]])


def test_solar_strength():
    _s = Solar(HEHE)
    # required to calculate the strength (divide by dist²)
    _s.fill_dist_mat()
    # ##
    expected_value = _s.G / 100
    expected_mat = [[0., expected_value],
                    [expected_value, 0.]]
    _s.fill_strength_mat()
    assert _s.strength_mat.shape == (2, 2)
    # atol + rtol * abs(desired)
    np.testing.assert_allclose(_s.strength_mat, expected_mat, rtol=1e-5, atol=0)
