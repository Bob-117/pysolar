# Planetary System

![Alt text](assets/made-with-python.svg)

[//]: # (![Alt Text]&#40;solar.gif&#41;)
<img src="assets/solar.gif" alt="drawing" width="300"/>

# Todo:

- [ ] fix first rotation
```shell
# back on screen after 52s
pos : 67.74--1.86 / v : -0.07-0.68
52.49
pos : 67.67--1.18 / v : -0.07-0.68
52.52
pos : 67.60--0.50 / v : -0.07-0.68
52.55
pos : 67.53-0.18 / v : -0.07-0.68
52.59
pos : 67.47-0.87 / v : -0.07-0.69
52.62
```
- [ ] implement solar system with planets list, generic gravitational_field_strength()
  - [X] handle planet list
  - [X] distance matrix
  - [X] strength matrix
  - [ ] move for p in list
  - [ ] plot for p in list
```shell
[['   Distance (m)' '         gibson' '         neelix' '       mordecai']
 ['         gibson' '               ' '       5.00E+01' '       1.00E+02']
 ['         neelix' '       5.00E+01' '               ' '       1.12E+02']
 ['       mordecai' '       1.00E+02' '       1.12E+02' '               ']]
[['   Strength (N)' '         gibson' '         neelix' '       mordecai']
 ['         gibson' '               ' '       2.67E-12' '       6.67E-13']
 ['         neelix' '       2.67E-12' '               ' '       5.34E-13']
 ['       mordecai' '       6.67E-13' '       5.34E-13' '               ']]
```
- [ ] find a good way to handle physics & consts (G, friction..)
- [ ] \+ real values (weight, dist, speed)
- [ ] side menu to control the model
- [ ] *use it to train a collision detection random forest ????
<br>
<br>
<br>
- [ ] move gif.py to python sandbox




